# -*- coding: utf-8 -*-

import sys
from argparse import ArgumentParser

from .utils import EnvManager
from .helper import RunOpts


def main(programm_args):
    main_parser = ArgumentParser(
        'py_env_utils',
        description=u"Утилиты для работы с окружением проекта"
    )
    main_parser.add_argument(
        '--verbose',
        default=False,
        action='store_true',
        help=u'Опция: подробный вывод'
    )
    main_parser.add_argument(
        '--stash',
        default=False,
        action='store_true',
        help=u'Опция: использовать ли stash при обновлении dev пакетов, '
             u'НА СВОЙ СТРАХ И РИСК!!!'
    )
    main_parser.add_argument(
        '--project-path',
        required=False,
        help=u'Опция: путь к проекту'
    )
    main_parser.add_argument(
        '--py-path',
        required=False,
        help=u'Опция: путь к питону, поставится в окружение'
    )
    main_parser.add_argument(
        '--env-path',
        required=False,
        help=u'Опция: путь к окружению проекта'
    )
    main_parser.add_argument(
        '--django-mode',
        action='store_true',
        help=u'Опция: ставить или нет manage.py скрипт'
    )
    main_parser.add_argument(
        '--download-packages-path',
        required=False,
        help=u'Опция: путь к каталогу для пакетов в режиме разработки'
    )
    main_parser.add_argument(
        '--create-env',
        action='store_true',
        help=u'Действие: создать пустое окружение'
    )
    main_parser.add_argument(
        '--install-packages',
        action='store_true',
        help=u'Действие: установить пакеты'
    )
    main_parser.add_argument(
        '--remove-packages',
        action='store_true',
        help=u'Действие: удалить пакеты'
    )
    main_parser.add_argument(
        '--packages',
        choices=(
            'not_dev',
            'all',
            'dev'
        ),
        default='not_dev',
        help=u'Опция: not_dev поставит пакеты, '
             u'которые не отмечены как "в режиме разработки", '
             u'all, ставит все пакеты,'
             u'dev ставит только пакеты, отмеченные как "в режиме разработки"'
    )
    main_parser.add_argument(
        '--remove-env',
        action='store_true',
        help=u'Действие: удалить окружение'
    )
    main_parser.add_argument(
        '--remove-pycs',
        action='store_true',
        help=u'Действие: удалить pyc файлы'
    )
    main_parser.add_argument(
        '--checkout-packages',
        action='store_true',
        help=u'Действие: актуализировать пакеты, '
             u'в режиме разработки будет выбрана нужная ревизия, '
             u'в противном случае pip попытается обновить пакет'
    )
    main_args = main_parser.parse_args(programm_args)

    run_opts = RunOpts(**{k: v for k, v in vars(main_args).items() if v is not None})

    package_type = 'all' if main_args.remove_env else main_args.packages

    env_manager = EnvManager.create_package_manager(
        packages=package_type,
        run_opts=run_opts
    )

    if main_args.create_env or main_args.install_packages:
        env_manager.create_env()

    if main_args.install_packages:
        env_manager.install_packages()

    if main_args.remove_packages or main_args.remove_env:
        env_manager.remove_packages()

    if main_args.remove_env:
        env_manager.remove_env()

    if main_args.checkout_packages:
        env_manager.checkout_packages()

    if main_args.remove_pycs:
        env_manager.remove_pycs()


def entry_point():
    if len(sys.argv) < 2:
        return
    main(sys.argv[1:])


if __name__ == '__main__':
    entry_point()
