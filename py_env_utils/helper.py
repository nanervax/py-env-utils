import os
import sys
from subprocess import Popen, PIPE
import logging
import traceback
import json

from . import settings


LOGGER = logging.getLogger('root')


class CallMixin(object):
    def _call(self, command):
        return _call(command, self.run_opts.verbose)


class RunOpts(object):
    def __init__(self, **kwargs):
        self.project_path = \
            kwargs.get('project_path', settings.DEFAULT_PROJECT_PATH)
        self.project_requirements_path = \
            os.path.join(self.project_path, 'requirements.txt')
        self.env_path = kwargs.get(
            'env_path', os.path.join(self.project_path + '-env')
        )
        self.download_packages_path = kwargs.get(
            'download_packages_path', self.project_path + '_packages'
        )
        self.verbose = kwargs.get('verbose', False)
        self.py_path = kwargs.get('py_path', '/usr/bin/python3')
        self.django_mode = kwargs.get('django_mode', False)
        self.use_stash = kwargs.get('stash', False)
        try:
            if os.path.exists(settings.DEPLOYMENT_FILENAME):
                with open(settings.DEPLOYMENT_FILENAME, 'r') as json_file:
                    self._deployment_opts = json.load(json_file)
            else:
                self._deployment_opts = {}
        except Exception:
            err_msg = "Error open/parse deployment file"
            if self.verbose:
                LOGGER.error("{}\n{}".format(err_msg, traceback.format_exc()))
            else:
                LOGGER.error("Error open/parse deployment file")
            raise SystemExit(1)

    @property
    def dev_packages(self):
        return self._deployment_opts.get('devPackages', [])

    @property
    def modules_to_py_path(self):
        return self._deployment_opts.get('modules', [])

    @property
    def pip_from_env(self):
        return os.path.join(self.env_path, 'bin', 'pip')

    @property
    def python_from_env(self):
        return os.path.join(self.env_path, 'bin', 'python')

    @property
    def env_bin_symlink(self):
        return os.path.join(os.path.join(self.project_path, 'bin'))

    @property
    def manage_script(self):
        return os.path.join(self.env_path, 'bin', 'manage')


class CallingException(Exception):
    pass


def _call(command, verbose=False):
    extra_params = {
        'stdout': PIPE,
        'stderr': PIPE
    }
    if not verbose:
        DEVNULL = open(os.devnull, 'wb')
        extra_params.update({
            'stderr': DEVNULL
        })
    p = Popen(command, shell=True, **extra_params)
    output, err = p.communicate()
    if verbose and output:
        if sys.version_info >= (3, 5):
            try:
                LOGGER.info(output.decode())
            except UnicodeDecodeError:
                LOGGER.info('Can not decode byte string, original in bytes: {}'.format(output))
        else:
            LOGGER.info(output)
    if p.returncode != 0:
        raise CallingException(
            'Can not exec command "{}"\nErr: {}'.format(command, err)
        )
    return output.strip()
