import os
import logging.config

DEFAULT_PROJECT_PATH = os.getcwd()
DEPLOYMENT_FILENAME = 'deployment.json'

MANAGE_SCRIPT_PATH = os.path.join(os.path.dirname(__file__), 'dj_manage_script')

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'simple_formatter': {
            'class': 'logging.Formatter',
            'format': '%(asctime)s: %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'simple_formatter',
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console']
    },
})
