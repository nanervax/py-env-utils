import os
from abc import abstractmethod
import shutil
import re
import functools
from logging import getLogger

from . import settings
from .helper import RunOpts, CallMixin, CallingException
from . import commands


LOGGER = getLogger('root')


def func_call_logger(name=None):
    def decorator(method):
        method_verbose_name = name or method.__name__

        @functools.wraps(method)
        def wrapper(self, *args, **kwargs):
            LOGGER.info('****** Start "{}" ******'.format(method_verbose_name))
            res = method(self, *args, **kwargs)
            LOGGER.info('****** End "{}" ******'.format(method_verbose_name))
            return res
        return wrapper
    return decorator


class EnvManager(CallMixin):
    def __init__(self, strategy, run_opts):
        self.run_opts = run_opts
        self.modules_python_path = '\n'.join([
            os.path.join(run_opts.project_path, module)
            for module in run_opts.modules_to_py_path
        ])
        self.package_strategy = strategy
        self.package_strategy.set_packages(self._prepare_packages())
        self.package_strategy.set_run_opts(self.run_opts)

    @classmethod
    def create_package_manager(cls, packages, *args, **kwargs):
        strategy = None
        if packages == 'not_dev':
            strategy = PackageManagerStrategy()
        if packages == 'all':
            strategy = ComposeManagerStrategy([
                PackageManagerStrategy(),
                DevPackageManagerStrategy()
            ])
        if packages == 'dev':
            strategy = DevPackageManagerStrategy()
        return cls(strategy, *args, **kwargs)

    def install_packages(self):
        self.package_strategy.install_packages()

    def remove_packages(self):
        self.package_strategy.remove_packages()

    def checkout_packages(self):
        self.package_strategy.checkout_packages()

    @func_call_logger(name='rm pycs')
    def remove_pycs(self):
        os.chdir(self.run_opts.project_path)
        self._call(commands.REMOVE_PYCS)
        if os.path.exists(self.run_opts.download_packages_path):
            os.chdir(self.run_opts.download_packages_path)
            self._call(commands.REMOVE_PYCS)

    @func_call_logger(name='create env dir')
    def create_env(self):
        env_path = self.run_opts.env_path
        if not os.path.exists(env_path):
            self._call(commands.CREATE_ENV.format(
                self.run_opts.py_path, env_path
            ))
            lib_dir = os.path.join(self.run_opts.env_path, 'lib')
            for file_name in os.listdir(lib_dir):
                full_path = os.path.join(lib_dir, file_name)
                if not os.path.isdir(full_path):
                    continue
                pth_file_path = os.path.join(
                    full_path,
                    'site-packages',
                    'project.pth'
                )
                with open(pth_file_path, 'w') as f:
                    f.write(self.modules_python_path)
        else:
            LOGGER.info('Env path already exists')
        if not os.path.exists(self.run_opts.env_bin_symlink):
            os.symlink(os.path.join(env_path, 'bin'), self.run_opts.env_bin_symlink)
        else:
            LOGGER.info('Symlink bin already exists')

        if self.run_opts.django_mode:
            env_manage_path = os.path.join(self.run_opts.env_path, 'bin', 'manage.py')
            with open(settings.MANAGE_SCRIPT_PATH, 'r') as src_manage:
                script_content = src_manage.read().strip()
                with open(env_manage_path, 'w') as dst_manage:
                    dst_manage.write(script_content.format(python=self.run_opts.python_from_env))
            os.chmod(env_manage_path, 0o755)

    @func_call_logger(name='delete env dir')
    def remove_env(self):
        env_path = self.run_opts.env_path
        if os.path.exists(self.run_opts.env_bin_symlink):
            os.unlink(self.run_opts.env_bin_symlink)
        else:
            LOGGER.info('Symlink bin already removed')
        if os.path.exists(env_path):
            shutil.rmtree(env_path)
        else:
            LOGGER.info('Env path already removed')

    def _prepare_packages(self):
        packages = []
        for line in open(self.run_opts.project_requirements_path, 'r'):
            stripped_line = line.strip()
            if not stripped_line or stripped_line.startswith('#'):
                continue
            packages.append(stripped_line)
        return packages


class AbstractPackageManagerStrategy(object):
    def set_packages(self, packages):
        pass

    def set_run_opts(self, run_opts):
        pass

    @abstractmethod
    def remove_packages(self):
        raise NotImplementedError

    @abstractmethod
    def install_packages(self):
        raise NotImplementedError

    @abstractmethod
    def checkout_packages(self):
        raise NotImplementedError


class ComposeManagerStrategy(AbstractPackageManagerStrategy):
    def __init__(self, strategies):
        self.strategies = strategies

    def set_packages(self, packages):
        for strategy in self.strategies:
            strategy.set_packages(packages)

    def set_run_opts(self, run_opts):
        for strategy in self.strategies:
            strategy.set_run_opts(run_opts)

    def remove_packages(self):
        for strategy in self.strategies:
            strategy.remove_packages()

    def install_packages(self):
        for strategy in self.strategies:
            strategy.install_packages()

    def checkout_packages(self):
        for strategy in self.strategies:
            strategy.checkout_packages()


class BasePackageManagerStrategy(AbstractPackageManagerStrategy, CallMixin):
    RE_GIT_REPOSITORY = re.compile(
        r'^(?P<url>(?:.*)/(?P<git_package_name>.+).git)@?'
        r'(?P<revision>[\.\w-]+)?#?(?:egg=)?(?P<pip_package_name>[\w-]*)$'
    )

    def __init__(self, run_opts=None):
        self.packages = None
        self.run_opts = run_opts or RunOpts()

    def set_packages(self, packages):
        self.packages = self._filter_packages(packages)

    def set_run_opts(self, run_opts):
        self.run_opts = run_opts

    def _filter_packages(self, packages):
        return list(filter(self.is_needed_package_predicate(), packages))

    def _parse_git_package(self, line):
        matches = self.RE_GIT_REPOSITORY.search(line)
        if not matches:
            return None
        result = matches.groupdict()
        if not result.get('pip_package_name'):
            result['pip_package_name'] = result['git_package_name']
        return result

    @abstractmethod
    def is_needed_package_predicate(self):
        raise NotImplementedError

    @abstractmethod
    def remove_packages(self):
        raise NotImplementedError

    @abstractmethod
    def install_packages(self):
        raise NotImplementedError

    @abstractmethod
    def checkout_packages(self):
        raise NotImplementedError


class PackageManagerStrategy(BasePackageManagerStrategy):
    RE_PACKAGE_NAME = re.compile(r'(?P<package_name>[\w-]+)[<|=!>]?')

    def is_needed_package_predicate(self):
        return lambda package: not any(
            [dev_package in package for dev_package in self.run_opts.dev_packages]
        )

    @func_call_logger(name='install packages')
    def install_packages(self):
        for package in self.packages:
            self._call(commands.NOT_DEV_PACKAGE_INSTALL.
                       format(self.run_opts.pip_from_env, package))

    @func_call_logger(name='remove packages')
    def remove_packages(self):
        if not os.path.exists(self.run_opts.pip_from_env):
            LOGGER.info("Env dir does not exists, aborting...")
            return
        for package in self.packages:
            if 'git+' in package:
                package_name = self._parse_git_package(package)['pip_package_name']
                if not package_name:
                    LOGGER.info('Unknown git line format "{}"'.format(package))
            else:
                match = self.RE_PACKAGE_NAME.match(package)
                if match:
                    package_name = match.groupdict()['package_name']
                else:
                    LOGGER.info('Unknown package line format "{}"'.format(package))
                    continue
            self._call(commands.NOT_DEV_PACKAGE_UNINSTALL.
                       format(self.run_opts.pip_from_env, package_name))

    @func_call_logger(name='checkout packages')
    def checkout_packages(self):
        for package in self.packages:
            LOGGER.info('Checkout package: "{}"'.format(package))
            self._call(commands.NOT_DEV_PACKAGE_UPDATE.
                       format(self.run_opts.pip_from_env, package))


class DevPackageManagerStrategy(BasePackageManagerStrategy):
    def is_needed_package_predicate(self):
        return lambda package: any(
            [dev_package in package for dev_package in self.run_opts.dev_packages]
        )

    def get_package_path(self, package):
        return os.path.join(
            self.run_opts.download_packages_path, package['git_package_name']
        )

    @property
    def git_packages(self):
        return [self._parse_git_package(package) for package in self.packages]

    @func_call_logger(name='install dev packages')
    def install_packages(self):
        if not os.path.exists(self.run_opts.download_packages_path) and self.git_packages:
            os.mkdir(self.run_opts.download_packages_path)

        new_packages = []
        for package in self.git_packages:
            package_path = self.get_package_path(package)
            if os.path.exists(package_path):
                LOGGER.info(
                    'Package {} already exists, skipping...'
                    .format(package['git_package_name'])
                )
                continue
            self._clone_package(package)
            new_packages.append(package)

        self._checkout_packages(new_packages)
        for package in new_packages:
            package_path = self.get_package_path(package)
            os.chdir(package_path)
            LOGGER.info(
                'Install package: "{}"'.format(package['pip_package_name'])
            )
            self._call(commands.DEV_PACKAGE_INSTALL.format(
                self.run_opts.pip_from_env
            ))

    @func_call_logger(name='remove dev packages')
    def remove_packages(self):
        if not os.path.exists(self.run_opts.pip_from_env):
            LOGGER.info('Env does not exists, nothing to delete from env...')
        else:
            for package in self.git_packages:
                LOGGER.info(
                    'Uninstall package: "{}"'.format(package['pip_package_name'])
                )
                self._call(
                    commands.DEV_PACKAGE_UNINSTALL
                    .format(self.run_opts.pip_from_env, package['pip_package_name'])
                )
        self._clean_packages_dir()

    def checkout_packages(self):
        self._checkout_packages(self.git_packages)

    @func_call_logger(name='checkout packages')
    def _checkout_packages(self, packages):
        for package in packages:
            LOGGER.info('***** Checkout revision for "{}"'
                        .format(package['git_package_name']))
            if not package['revision']:
                LOGGER.info(
                    'No revision in requirements for "{}", checkout on master'
                    .format(package['git_package_name'])
                )
                self._checkout_package(package, 'master')
                LOGGER.info('***** Success checkout revision for "{}"'
                            .format(package['git_package_name']))
                continue

            self._checkout_package(package, package['revision'])
            LOGGER.info('***** Success checkout revision for "{}"'
                        .format(package['git_package_name']))

    def _checkout_package(self, package, revision):
        os.chdir(self.get_package_path(package))
        stash_is_using = False
        if self.run_opts.use_stash:
            if self._call(commands.GIT_DIFF):
                stash_is_using = True
                LOGGER.info('Hide working dir into stash')
                self._call(commands.GIT_STASH)
            else:
                LOGGER.info('Nothing to hide in stash, skipping stash using...')

        self._checkout_revision(revision='master')
        LOGGER.info('Pull branch "master"')
        self._call(commands.GIT_PULL)
        LOGGER.info('Pull all tags')
        self._call(commands.GIT_PULL_ALL_TAGS)

        if self._equal_revisions(revision):
            LOGGER.info(
                'Revisions of {} are equal'
                .format(package['git_package_name'])
            )
            if stash_is_using:
                self._stash_pop()
            return

        self._checkout_revision(revision=revision)
        LOGGER.info('Pull revision "{}"'.format(revision))
        try:
            self._call(commands.GIT_PULL)
        except CallingException:
            LOGGER.info('This is tag, can not pull on tag: "{}"'.format(revision))
        if stash_is_using:
            self._stash_pop()

    def _stash_pop(self):
        LOGGER.info('Pop working dir from stash')
        self._call(commands.GIT_STASH_POP)

    def _checkout_revision(self, revision):
        if not os.path.exists(self.run_opts.download_packages_path):
            LOGGER.warning('Dev packages path does not exists, aborting...')
            return
        LOGGER.info('Checkout revision on "{}"'.format(revision))
        self._call(commands.GIT_CHECKOUT.format(revision))

    def _equal_revisions(self, revision):
        LOGGER.info('Checking for equal revisions...')
        LOGGER.info('Revision in requirements')
        try:
            requirements_revision_hash = \
                self._call(commands.GIT_REV_PARSE.format(revision))
        except CallingException:
            LOGGER.info('Can not checking revision, may be it is new package?')
            return False
        LOGGER.info('Current revision')
        current_revision_hash = self._call(commands.GIT_REV_PARSE.format('HEAD'))
        return requirements_revision_hash == current_revision_hash

    @func_call_logger(name='clean dev packages dir')
    def _clean_packages_dir(self):
        LOGGER.info('Clear dev packages dir')
        for package in self.git_packages:
            package_path = os.path.join(
                self.run_opts.download_packages_path, package['git_package_name']
            )
            if not os.path.exists(package_path):
                LOGGER.info('package_path for "{}" does not exist.. skipping'
                            .format(package['git_package_name']))
                continue
            shutil.rmtree(package_path)
        shutil.rmtree(self.run_opts.download_packages_path)

    @func_call_logger(name='clone dev packages')
    def _clone_package(self, package):
        os.chdir(self.run_opts.download_packages_path)
        LOGGER.info('Cloning package from url "{}" into "{}"'.format(
            package['url'], os.path.join(self.run_opts.download_packages_path, package['git_package_name'])
        ))
        self._call(commands.GIT_CLONE.format(package['url']))
