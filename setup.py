from os import path

from setuptools import setup, find_packages


setup(
    name='py_env_utils',
    version='3.0.0',
    packages=find_packages(),
    include_package_data=True,
    long_description=open(path.join(path.dirname(__file__), 'README.md')).read(),
    python_requires='>=2.7',
    entry_points={
        'console_scripts': [
            'py_env_utils = py_env_utils.run:entry_point',
        ],
    }
)
